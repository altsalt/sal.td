# Sal.td: canonical IndieWeb identity of Wm Salt Hale <[@sal.td](https://sal.td)> aka <[@altsalt(.net)](www.altsalt.net)>

Built on top of [Eleventy] and [Parcel], initially based on [eleventy-parcel-boilerplate] and now moving [XITY]; a blog-ready [11ty] starter based on [PostCSS] and [Parcel] with a RSS feed.

[Eleventy] is <cite>_a simpler static site generator_</cite>, which does a beautiful job of scaffolding a static site. However, a web application is so much more; what about images, stylesheets, or scripts? This is where [Parcel], a <cite>_zero configuration web application bundler_</cite>, comes in. By combining [Eleventy] with [Parcel], static sites can be taken to the next level with minimal effort.

## Key Features

- [Eleventy][] for templates and site generation
- [PostCSS][] and [PostCSS Preset Env][] to process CSS
- [cssnano][] to minimize, merge, and optimize the CSS output
- [Parcel][] for a simple asset build pipeline

## Prerequisites

- [Node.js][] & npm
- [Yarn][]

---

## Usage

### Installation

Clone the repository and enter its directory, then run this command:

```bash
yarn install
```

### Development

To start the development server, enter the following command which will run [parcel] and open a local server (including [browser-sync][]) with auto reload.

```bash
yarn start
```

### Production

To generate the static site, enter the following command which will prepare the content assets and run optimizations for a production release.

```bash
yarn build
```

### Additional Commands

XITY provides also two more commands useful to update dependencies and the cssdb used by [Browserslist] and [PostCSS]:

```bash
# Update dependencies interactively
yarn update:deps

# Update the cssdb definitions. Run it every month.
yarn update:cssdb
```

## Configuration

### Content

Content of the site lives in the `src/` directory by default.

### SSG Settings

Configure the static site generator by changing the settings inside `src/_data/config.json`.

### `package.json`

The `browserslist` property reflects the browsers the static website supports, per [Browserslist].

### `.babelrc`

The [Babel] smart preset allows us to use the latest JavaScript. Two separate plugins supporting (private) class methods and properties are added by default as well.

### `.eslintrc` and `src/.eslintrc`

This project follows [Airbnb] configuration for [ESLint]. The source directory extends the base configuration, and makes sure `process` can be used in the JavaScript, as this is [supported][1] by [Parcel].

Linting is run on the configuration files, as well as the scripts in the source directory of the static site.

### `.stylelintrc`

This project follows the recommended configuration for [stylelint], with support for SCSS-syntax. Linting is run as part of [PostCSS] as explained below.

### `.eleventy.js`

The [Eleventy] configuration file adds support for running a staging environment.

In addition, it sets some sane defaults, as well as providing a boilerplate for how to add custom filters and tags.

### `.postcssrc`

The [PostCSS] configuration adds a number of plugins. Stylesheets are linted with [stylelint], before being optimized with [PostCSS Preset Env].

[airbnb]: https://github.com/airbnb/javascript
[babel]: https://babeljs.io/
[browser-sync]: https://www.npmjs.com/package/browser-sync 'Time-saving synchronised browser testing'
[Browserslist]: https://github.com/browserslist/browserslist
[cssnano]: https://cssnano.co 'A modular minifier based on the PostCSS ecosystem'
[11ty]: https://11ty.dev 'Static site generator'
[eleventy]: https://11ty.dev 'Static site generator'
[eleventy-parcel-boilerplate]: https://github.com/vseventer/eleventy-parcel-boilerplate
[eslint]: https://eslint.org/
[node.js]: https://nodejs.org/
[parcel]: https://parceljs.org 'Web application bundler'
[postcss]: https://postcss.org 'A tool for transforming CSS with JavaScript'
[PostCSS Preset Env]: https://preset-env.cssdb.org 'Use tomorrow’s CSS today'
[stylelint]: https://stylelint.io/
[XITY]: https://github.com/equinusocio/xity-starter
[yarn]: https://yarnpkg.com/ 'Package Manager'

[1]: https://parceljs.org/env.html

## TODO
- [ ] decide on list of things to highlight
- [ ] decide on which profiles to link to
  - [x] add google scholar
  - [x] add orcid
- [ ] fix scaling on profiles
- [ ] test at various responsive scales
- [ ] add language to align with linkedin bridge building
- [ ] add link to resume and cv
- [ ] fold in upstream updates
- [ ] decide what to do regarding 404 as this is a single page site
- [ ] strip unused components from codebase

## Reference

## License
Copyright (c) 2021 Wm Salt Hale

The documentation provided for this project is released under a Creative Commons Attribution-ShareAlike 4.0 International License (CC BY-SA 4.0) https://creativecommons.org/licenses/by-sa/4.0/. The code provided for this project is released under the GNU General Public License version 3 (GNU GPLv3) https://www.gnu.org/licenses/gpl-3.0.

